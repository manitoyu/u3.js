import { expect } from 'chai'
import Vector3 from '../../src/core/Vector3'

describe('Vector3', () => {      
  it('new', () => {
    const v = new Vector3(1, 2, 3)
    expect(v.x).to.equal(1)
    expect(v.y).to.equal(2)
    expect(v.z).to.equal(3)
  })

  it('equal', () => {
    expect(new Vector3(1, 2, 3).equal(new Vector3(1, 2, 3))).to.be.true
  })

  it('add', () => {
    expect(new Vector3(1, 2, 3).add(new Vector3(1, 2, 3)).equal(new Vector3(2, 4, 6))).to.be.true
  })

  it('subtract', () => {
    expect(new Vector3(2, 4, 6).subtract(new Vector3(1, 2, 3)).equal(new Vector3(1, 2, 3))).to.be.true
  })

  it('multiply', () => {
    expect(new Vector3(1, 2, 3).multiply(2).equal(new Vector3(2, 4, 6))).to.be.true
  })

  it('divide', () => {
    expect(new Vector3(2, 4, 6).divide(2).equal(new Vector3(1, 2, 3))).to.be.true
  })

  it('addSelf', () => {
    const vec = new Vector3(1, 2, 3)
    vec.addSelf(new Vector3(1, 2, 3))
    expect(vec.x).to.eq(2)
    expect(vec.y).to.eq(4)
    expect(vec.z).to.eq(6)
  })

  it('subtractSelf', () => {
    const vec = new Vector3(2, 4, 6)
    vec.subtractSelf(new Vector3(1, 2, 3))
    expect(vec.x).to.eq(1)
    expect(vec.y).to.eq(2)
    expect(vec.z).to.eq(3)
  })

  it('multiplySelf', () => {
    const vec = new Vector3(1, 2, 3)
    vec.multiplySelf(2)
    expect(vec.x).to.eq(2)
    expect(vec.y).to.eq(4)
    expect(vec.z).to.eq(6)
  })

  it('divideSelf', () => {
    const vec = new Vector3(2, 4, 6)
    vec.divideSelf(2)
    expect(vec.x).to.eq(1)
    expect(vec.y).to.eq(2)
    expect(vec.z).to.eq(3)
  })

  it('oppsite', () => {
    expect(new Vector3(1, 2, 3).oppsite().equal(new Vector3(-1, -2, -3))).to.be.true
  })

  it('normalize', () => {
    const vec = new Vector3(3, 4, 5)
    const mag = Math.sqrt(3 ** 2 + 4 ** 2 + 5 ** 2)

    vec.normalize()

    expect(vec.x).to.eq(3 / mag)
    expect(vec.y).to.eq(4 / mag)
    expect(vec.z).to.eq(5 / mag)
  })

  it('dot', () => {
    expect(new Vector3(1, 2, 3).dot(new Vector3(1, 2, 3))).to.be.eq(14)
  })

  it('cross', () => {
    expect(new Vector3(1, 2, 3).cross(new Vector3(4, 5, 6)).equal(new Vector3(-3, 6, -3))).to.be.true
  })

  it('distance', () => {
    expect(new Vector3(1, 0, 0).distance(new Vector3(0, 1, 0))).to.be.eq(Math.sqrt(2))
  })

  it('magnitude', () => {
    expect(new Vector3(3, 4, 0).magnitude).to.be.eq(5)
  })

  it('normailzed', () => {
    expect(new Vector3(1, 0, 0).normalize().equal(new Vector3(1, 0, 0))).to.be.true
  })

  it('zero vector calls normalize should throw error', () => {
    expect(() => Vector3.zero.normalize()).to.throw(Error)
  })
})