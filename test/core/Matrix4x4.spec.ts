import { expect } from 'chai'
import { equalf } from '../../src/utils/utils'
import Matrix4x4 from '../../src/core/Matrix4x4'
import Vector4 from '../../src/core/Vector4'
import Quaternion from '../../src/core/Quaternion'

describe('Matrix4x4', () => {
  const mat = Matrix4x4.perspective(120, 1, 1, 9)

  // it('multiplyVector4', () => {
  //   const vec = new Vector4(.5, .5, .5, 1)
  //   const result = mat.multiplyVector4(vec)

  //   expect(result.equal(new Vector4(0.2886751, 0.2886751, -2.875, -0.5))).to.ok
  // })

  // it('multiplyMatrix', () => {
  //   const result = mat.multiplyMatrix(mat)

  //   expect(equalf(result.m11, 0.3333)).to.ok
  //   expect(equalf(result.m12, 0)).to.ok
  //   expect(equalf(result.m13, 0)).to.ok
  //   expect(equalf(result.m14, 0)).to.ok

  //   expect(equalf(result.m21, 0)).to.ok
  //   expect(equalf(result.m22, 0.333333)).to.ok
  //   expect(equalf(result.m23, 0)).to.ok
  //   expect(equalf(result.m24, 0)).to.ok

  //   expect(equalf(result.m31, 0)).to.ok
  //   expect(equalf(result.m32, 0)).to.ok
  //   expect(equalf(result.m33, 3.8125)).to.ok
  //   expect(equalf(result.m34, 2.8125)).to.ok

  //   expect(equalf(result.m41, 0)).to.ok
  //   expect(equalf(result.m42, 0)).to.ok
  //   expect(equalf(result.m43, 1.25)).to.ok
  //   expect(equalf(result.m44, 2.25)).to.ok
  // })

  it('fromQuaternion', () => {
    const mat = Matrix4x4.fromQuaternion(Quaternion.euler(30, 30, 30))
  })
})