import Vector3 from '../core/Vector3'
import Vector2 from '../core/Vector2'
import { Bresenham } from '../core/Algorithm'
import { clamp, lerp } from '../utils/utils'
import { VertexOut, Triangle } from '../core/Structs'
import Camera from '../game-objects/Camera'

const { abs } = Math

class RasterizerWorker {
  lerp(fragment: VertexOut[], cb: (o: any) => void) {
    const vertices = fragment.map(vertex => ({
      screenPoint: Camera.main.viewportToScreenPoint(
        new Vector3(vertex.position.x / vertex.position.w, vertex.position.y / vertex.position.w, vertex.position.w)),
      vertex
    }))
    
    const sortedVertices = vertices.slice().sort((a, b) => a.screenPoint.y <= b.screenPoint.y ? -1 : 1)

    const A = sortedVertices[0].screenPoint
    const B = sortedVertices[1].screenPoint
    const C = sortedVertices[2].screenPoint

    const at = sortedVertices[0].vertex.texcoord
    const bt = sortedVertices[1].vertex.texcoord
    const ct = sortedVertices[2].vertex.texcoord

    const kAB = (B.y - A.y) / (B.x - A.x)
    const kBC = (C.y - B.y) / (C.x - B.x)
    const kAC = (C.y - A.y) / (C.x - A.x)
    
    // 背面剔除
    const wn = vertices[0].screenPoint.subtract(vertices[1].screenPoint)
      .cross(vertices[2].screenPoint.subtract(vertices[1].screenPoint))
      .normalize()

    // 插值方向
    const sn = B.subtract(A).cross(C.subtract(B)).normalize()

    let a: number = 0
    let b: number = 0

    let lt: Vector2
    let rt: Vector2

    let lz: number
    let rz: number

    for (let i = A.y; i < C.y; i ++) {
      if (i < B.y) {
        b = A.x + (abs(kAB) == Infinity ? 0 : 1 / kAB) * (i - A.y)
        lz = 1 / lerp(1 / A.z, 1 / B.z, (i - A.y) / (B.y - A.y))
        lt = Vector2.lerp(at.multiply(1 / A.z), bt.multiply(1 / B.z), (i - A.y) / (B.y - A.y)).multiply(lz)
      } else {
        b = B.x + (abs(kBC) == Infinity ? 0 : 1 / kBC) * (i - B.y)
        lz = 1 / lerp(1 / B.z, 1 / C.z, (i - B.y) / (C.y - B.y))
        lt = Vector2.lerp(bt.multiply(1 / B.z), ct.multiply(1 / C.z), (i - B.y) / (C.y - B.y)).multiply(lz)
      }

      a = A.x + (abs(kAC) == Infinity ? 0 : 1 / kAC) * (i - A.y)
      rz = 1 / lerp(1 / A.z, 1 / C.z, (i - A.y) / (C.y - A.y))
      rt = Vector2.lerp(at.multiply(1 / A.z), ct.multiply(1 / C.z), (i - A.y) / (C.y - A.y)).multiply(rz)

      Bresenham(Math.round(a), i, Math.round(b), i, (x, y, t) => {
        const z = 1 / lerp(1 / lz, 1 / rz, sn.z < 0 ? t : 1 - t)

        if (wn.z < 0) return
        
        const uv = Vector2.lerp(lt.multiply(1 / lz), rt.multiply(1 / rz), sn.z < 0 ? t : 1 - t).multiply(z)

        uv.x = clamp(uv.x, 0, 1)
        uv.y = clamp(uv.y, 0, 1)

        cb({ position: { x, y, z }, texcoord: uv, normal: vertices[0].vertex.normal })
      })
    }
  }
}

export default RasterizerWorker