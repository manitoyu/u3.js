import Vector2 from '../core/Vector2'
import Vector3 from '../core/Vector3'
import Vector4 from '../core/Vector4'
import { clamp } from '../utils/utils'
import { VertexOut, Vertex, Triangle } from '../core/Structs'

class Clipper {
  clip(vertexOuts: VertexOut[]): VertexOut[][] {
    const go = (vertexs: VertexOut[], clip: number): VertexOut[] => {
      if (clip == 64) return vertexs
      let nextVertexs: VertexOut[] = []
      for (let i = 0; i < vertexs.length; i ++) {
        nextVertexs = [
          ...nextVertexs,
          ...this.getVertexsByEndpoints(
            vertexs[i],
            vertexs[i].position,
            vertexs[(i + 1) % vertexs.length],
            vertexs[(i + 1) % vertexs.length].position,
            clip
          )
        ]
      }
      return go(nextVertexs, clip << 1)
    } 

    // FIXME 这里存在误差累计，得到的点可能依然在屏幕之外
    const vertexs = go(vertexOuts, 1)

    return this.getTrianglesByVertexs(vertexs)
  }

  // 多边形切分三角形
  private getTrianglesByVertexs(vertexs: VertexOut[]): VertexOut[][] {
    const triangles: VertexOut[][] = []

    for (let i = 0; i < vertexs.length - 2; i ++) {
      triangles.push([
        vertexs[0],
        vertexs[i + 1],
        vertexs[i + 2]
      ])
    }

    return triangles
  }

  // 根据端点和相交面，三角形切分为多边形
  private getVertexsByEndpoints(a: VertexOut, aClip: Vector4, b: VertexOut, bClip: Vector4, clip: number): VertexOut[] {
    const aCode = this.getAreaCode(aClip)
    const bCode = this.getAreaCode(bClip)

    // 内 -> 内
    if ((aCode & clip | bCode & clip) == 0) {
      return [b]
    }

    // 外 -> 外
    if ((aCode & clip & bCode & clip) == clip) {
      return []
    }

    let u: number = undefined

    if ((aCode & clip | bCode & clip) == clip) {
       if (clip == 1) {
        u = (aClip.x + aClip.w) / (aClip.w - bClip.w + aClip.x - bClip.x)
      }

      if (clip == 2) {
        u = (aClip.x - aClip.w) / (bClip.w - aClip.w + aClip.x - bClip.x)
      }

      if (clip == 4) {
        u = (aClip.y + aClip.w) / (aClip.w - bClip.w + aClip.y - bClip.y)
      }

      if (clip == 8) {
        u = (aClip.y - aClip.w) / (bClip.w - aClip.w + aClip.y - bClip.y)
      }

      if (clip == 16) {
        u = (aClip.z + aClip.w) / (aClip.w - bClip.w + aClip.z - bClip.z)
      }

      if (clip == 32) {      
        u = (aClip.z - aClip.w) / (bClip.w - aClip.w + aClip.z - bClip.z)
      }
    }

    const p = Vector4.lerp(aClip, bClip, u)

    // if (p.w == 0) return []

    // FIXME
    const intersection: VertexOut = {
      color: a.color,
      position: p,
      normal: a.normal,
      texcoord: Vector2.lerp(a.texcoord, b.texcoord, u)
    }

    // 内 -> 外
    if ((aCode & clip) == 0) {
      return [intersection]
    }

    // 外 -> 内
    if ((bCode & clip) == 0) {
      return [intersection, b]
    }
  }

  // 根据其次坐标算区域码
  private getAreaCode(position: Vector4): number {
    let code = 0

    if (position.w + position.x < 0) {
      code |= 1
    }
    if (position.w - position.x < 0) {
      code |= 2
    }
    if (position.w + position.y < 0) {
      code |= 4
    }
    if (position.w - position.y < 0) {
      code |= 8
    }
    if (position.w + position.z < 0) {
      code |= 16
    }
    if (position.w - position.z < 0) {
      code |= 32
    }

    return code
  }
}

export default Clipper