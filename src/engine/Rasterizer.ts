import Vector3 from '../core/Vector3'
import Vector2 from '../core/Vector2'
import { clamp, lerp } from '../utils/utils'
import { VertexOut, Triangle } from '../core/Structs'
import Camera from '../game-objects/Camera'
import Shader from '../components/Shader'
import Color from '../core/Color'

const { abs, round } = Math

class Rasterizer {
  zBuffer: number[]
  frameBuffer: Uint8ClampedArray
  msaaColorBuffer: Uint8ClampedArray

  lerp(fragment: VertexOut[]) {
    const vertices = fragment.map(vertex => ({
      screenPoint: Camera.main.viewportToScreenPoint(
        new Vector3(vertex.position.x / vertex.position.w, vertex.position.y / vertex.position.w, vertex.position.w)),
      vertex
    }))
    
    const sortedVertices = vertices.slice().sort((a, b) => a.screenPoint.y <= b.screenPoint.y ? -1 : 1)

    const A = sortedVertices[0].screenPoint
    const B = sortedVertices[1].screenPoint
    const C = sortedVertices[2].screenPoint

    const at = sortedVertices[0].vertex.texcoord
    const bt = sortedVertices[1].vertex.texcoord
    const ct = sortedVertices[2].vertex.texcoord


    const kAB = (B.y - A.y) / (B.x - A.x)
    const kBC = (C.y - B.y) / (C.x - B.x)
    const kAC = (C.y - A.y) / (C.x - A.x)
    
    // 背面剔除
    const wn = vertices[0].screenPoint.subtract(vertices[1].screenPoint)
      .cross(vertices[2].screenPoint.subtract(vertices[1].screenPoint))
      .normalize()

    if (wn.z < 0) return

    // 插值方向
    const sn = B.subtract(A).cross(C.subtract(B)).normalize()

    let a: number = 0
    let b: number = 0

    let lt: Vector2
    let rt: Vector2

    let lz: number
    let rz: number

    let uv: Vector2
    let color: Color

    let sx = 0
    let sy = 0

    let t: number
    let z: number

    let x0: number
    let x1: number

    // y插值
    for (let y = A.y; y < C.y; y ++) {
      if (y < B.y) {
        b = A.x + (abs(kAB) == Infinity ? 0 : 1 / kAB) * (y - A.y)
        lz = 1 / lerp(1 / A.z, 1 / B.z, (y - A.y) / (B.y - A.y))
        lt = Vector2.lerp(at.multiply(1 / A.z), bt.multiply(1 / B.z), (y - A.y) / (B.y - A.y)).multiply(lz)
      } else {
        b = B.x + (abs(kBC) == Infinity ? 0 : 1 / kBC) * (y - B.y)
        lz = 1 / lerp(1 / B.z, 1 / C.z, (y - B.y) / (C.y - B.y))
        lt = Vector2.lerp(bt.multiply(1 / B.z), ct.multiply(1 / C.z), (y - B.y) / (C.y - B.y)).multiply(lz)
      }

      a = A.x + (abs(kAC) == Infinity ? 0 : 1 / kAC) * (y - A.y)
      rz = 1 / lerp(1 / A.z, 1 / C.z, (y - A.y) / (C.y - A.y))
      rt = Vector2.lerp(at.multiply(1 / A.z), ct.multiply(1 / C.z), (y - A.y) / (C.y - A.y)).multiply(rz)

      x0 = round(a)
      x1 = round(b)

      if (x0 > x1) {
        x0 = x1 + x0
        x1 = x0 - x1
        x0 = x0 - x1
      }

      // x插值
      for (let x = x0; x <= x1; x ++) {
        t = sn.z < 0 ? (x - x0) / (x1 - x0) : 1 - (x - x0) / (x1 - x0)
        z = 1 / lerp(1 / lz, 1 / rz, t)

        if (this.zBuffer[(y * Camera.main.pixelWidth + x) * 4] > z) {
          this.zBuffer[(y * Camera.main.pixelWidth + x) * 4] = z

          uv = Vector2.lerp(lt.multiply(1 / lz), rt.multiply(1 / rz), t).multiply(z)
          uv.x = clamp(uv.x, 0, 1)
          uv.y = clamp(uv.y, 0, 1)

          color = Shader.main.surfaceProgram({ normal: vertices[0].vertex.normal, texcoord: uv })
          
          const index = (round((1 - y / 600) * 599) * Camera.main.pixelWidth + round(x / 800 * 799)) * 4
            
          this.frameBuffer[index] = 255 * color.r
          this.frameBuffer[index + 1] = 255 * color.g
          this.frameBuffer[index + 2] = 255 * color.b
          this.frameBuffer[index + 3] = 255 * color.a

          // sample1
          // {
          //   sx = x - 3 * .125
          //   sy = y + .125

          //   t = sn.z < 0 ? (sx - x0) / (x1 - x0) : 1 - (sx - x0) / (x1 - x0)

          //   if (t >= 0 && t <= 1) {
          //     z = 1 / lerp(1 / lz, 1 / rz, t)
          //     const index = (y * Camera.main.pixelWidth + x) * 4 + 0
          //     if (this.zBuffer[index] > z) {
          //       this.zBuffer[index] = z
          //       this.msaaColorBuffer[index * 4] = color.r * 255
          //       this.msaaColorBuffer[index * 4 + 1] = color.g * 255
          //       this.msaaColorBuffer[index * 4 + 2] = color.b * 255
          //       this.msaaColorBuffer[index * 4 + 3] = color.a * 255
          //     }
          //   } 
          // }

          // // sample2
          // {
          //   sx = x + 3 * .125
          //   sy = y - .125

          //   t = sn.z < 0 ? (sx - x0) / (x1 - x0) : 1 - (sx - x0) / (x1 - x0)

          //   if (t >= 0 && t <= 1) {
          //     z = 1 / lerp(1 / lz, 1 / rz, t)
          //     const index = (y * Camera.main.pixelWidth + x) * 4 + 1
          //     if (this.zBuffer[index] > z) {
          //       this.zBuffer[index] = z
          //       this.msaaColorBuffer[index * 4] = color.r * 255
          //       this.msaaColorBuffer[index * 4 + 1] = color.g * 255
          //       this.msaaColorBuffer[index * 4 + 2] = color.b * 255
          //       this.msaaColorBuffer[index * 4 + 3] = color.a * 255
          //     }
          //   }
          // }

          // // sample3
          // {
          //   sx = x + .125
          //   sy = y - 3 * .125

          //   t = sn.z < 0 ? (sx - x0) / (x1 - x0) : 1 - (sx - x0) / (x1 - x0)
          //   if (t >= 0 && t <= 1) {
          //     z = 1 / lerp(1 / lz, 1 / rz, t)
          //     const index = (y * Camera.main.pixelWidth + x) * 4 + 2
          //     if (this.zBuffer[index] > z) {
          //       this.zBuffer[index] = z
          //       this.msaaColorBuffer[index * 4] = color.r * 255
          //       this.msaaColorBuffer[index * 4 + 1] = color.g * 255
          //       this.msaaColorBuffer[index * 4 + 2] = color.b * 255
          //       this.msaaColorBuffer[index * 4 + 3] = color.a * 255
          //     }
          //   }
          // }

          // // sample4
          // {
          //   sx = x - .125
          //   sy = y + 3 * .125

          //   t = sn.z < 0 ? (sx - x0) / (x1 - x0) : 1 - (sx - x0) / (x1 - x0)
          //   if (t >= 0 && t <= 1) {
          //     z = 1 / lerp(1 / lz, 1 / rz, t)
          //     const index = (y * Camera.main.pixelWidth + x) * 4 + 3
          //     if (this.zBuffer[index] > z) {
          //       this.zBuffer[index] = z
          //       this.msaaColorBuffer[index * 4] = color.r
          //       this.msaaColorBuffer[index * 4 + 1] = color.g
          //       this.msaaColorBuffer[index * 4 + 2] = color.b
          //       this.msaaColorBuffer[index * 4 + 3] = color.a
          //     }
          //   }
          // }
        }
      }
    }
  }
}

export default Rasterizer


// 边缘
// if (false) {

// } else {
//   uv = Vector2.lerp(lt.multiply(1 / lz), rt.multiply(1 / rz), t).multiply(z)
//   uv.x = clamp(uv.x, 0, 1)
//   uv.y = clamp(uv.y, 0, 1)

//   color = Shader.main.surfaceProgram({ normal: vertices[0].vertex.normal, texcoord: uv })
// }

// const index = (round((1 - y / 600) * 599) * Camera.main.pixelWidth + round(x / 800 * 799)) * 4

// this.imageData[index] = 255 * color.r
// this.imageData[index + 1] = 255 * color.g
// this.imageData[index + 2] = 255 * color.b
// this.imageData[index + 3] = 255 * color.a