import Rasterizer from './Rasterizer'
import Clipper from './Clipper'
import GameObject from '../game-objects/GameObject'
import { flatMap } from '../utils/funtional'
import Camera from '../game-objects/Camera'
import Shader from '../components/Shader'
import Scene from '../game-objects/Scene'
import { VertexIn, VertexOut, Vertex, Triangle } from '../core/Structs'
import Vector4 from '../core/Vector4'
import Time from '../components/Time'
import Input from './Input'

class Engine {
  private rasterizer = new Rasterizer()
  private clipper = new Clipper()
  private frame: ImageData
  private zBuffer: number[]
  private frameBuffer: Uint8ClampedArray
  private msaaColorBuffer: Uint8ClampedArray
  private loadedScene: Scene
  private isStarted: boolean = false
  private isPaused: boolean = false
  private animationId: number
  private input: Input

  imageContext: CanvasRenderingContext2D

  constructor(canvas: HTMLCanvasElement) {
    this.imageContext = canvas.getContext('2d')

    const width = this.imageContext.canvas.width
    const height = this.imageContext.canvas.height

    this.zBuffer = new Array(width * 2 * height * 2)
    this.frame = this.imageContext.createImageData(width, height)
    this.frameBuffer = this.frame.data
    this.msaaColorBuffer = this.imageContext.createImageData(width * 2, height * 2).data

    this.rasterizer.zBuffer = this.zBuffer
    this.rasterizer.frameBuffer = this.frameBuffer
    this.rasterizer.msaaColorBuffer = this.msaaColorBuffer

    this.input = new Input(canvas)
  }

  submit(gameObject: GameObject) {
    const triangles = gameObject.submit()

    const localToWorldMatrix = gameObject.transform.localToWorldMatrix

    Shader.main.uniform = {
      MVMatrix: Camera.main.worldToCameraMatrix.multiplyMatrix(localToWorldMatrix),
      MVPMatrix: Camera.main.worldToViewportMatrix.multiplyMatrix(localToWorldMatrix)
    }

    const clippedFragment: VertexOut[][] = flatMap(triangles, triangle => {
      return this.clipper.clip([
        Shader.main.vertexProgram({
          position: new Vector4(triangle.a.position.x, triangle.a.position.y, triangle.a.position.z, 1),
          texcoord: triangle.a.texcoord,
          normal: triangle.a.normal
        }),
        Shader.main.vertexProgram({
          position: new Vector4(triangle.b.position.x, triangle.b.position.y, triangle.b.position.z, 1),
          texcoord: triangle.b.texcoord,
          normal: triangle.b.normal
        }),
        Shader.main.vertexProgram({
          position: new Vector4(triangle.c.position.x, triangle.c.position.y, triangle.c.position.z, 1),
          texcoord: triangle.c.texcoord,
          normal: triangle.c.normal
        })
      ])
    })

    clippedFragment.forEach(fragment => this.rasterizer.lerp(fragment))
  }

  render(scene: Scene) {
    this.rasterizer.zBuffer.fill(Camera.main.farClipPlane)
    this.rasterizer.frameBuffer.fill(0)
    // this.rasterizer.msaaColorBuffer.fill(0)

    scene.gameObjects.forEach(gameObject => {
      gameObject.scripts.forEach(script => script.update())
      gameObject.scripts.forEach(script => script.lateUpdate())
      this.submit(gameObject)
    })

    // for (let i = 0; i < this.frameBuffer.length / 4; i ++) {
    //   this.frameBuffer[i * 4] = (
    //       this.msaaColorBuffer[(i * 4) * 4]
    //     + this.msaaColorBuffer[(i * 4 + 1) * 4]
    //     + this.msaaColorBuffer[(i * 4 + 2) * 4]
    //     + this.msaaColorBuffer[(i * 4 + 3) * 4]) / 4
      
    //   this.frameBuffer[i * 4 + 1] = (
    //       this.msaaColorBuffer[(i * 4) * 4 + 1]
    //     + this.msaaColorBuffer[(i * 4 + 1) * 4 + 1]
    //     + this.msaaColorBuffer[(i * 4 + 2) * 4 + 1]
    //     + this.msaaColorBuffer[(i * 4 + 3) * 4 + 1]) / 4

    //   this.frameBuffer[i * 4 + 2] = (
    //       this.msaaColorBuffer[(i * 4) * 4 + 2]
    //     + this.msaaColorBuffer[(i * 4 + 1) * 4 + 2]
    //     + this.msaaColorBuffer[(i * 4 + 2) * 4 + 2]
    //     + this.msaaColorBuffer[(i * 4 + 3) * 4 + 2]) / 4

    //   this.frameBuffer[i * 4 + 3] = (
    //       this.msaaColorBuffer[(i * 4) * 4 + 3]
    //     + this.msaaColorBuffer[(i * 4 + 1) * 4 + 3]
    //     + this.msaaColorBuffer[(i * 4 + 2) * 4 + 3]
    //     + this.msaaColorBuffer[(i * 4 + 3) * 4 + 3]) / 4
    // }

    this.imageContext.putImageData(this.frame, 0, 0)
  }

  loadScene(scene: Scene) {
    this.loadedScene = scene
  }

  start() {
    if (this.isStarted) return
    this.isStarted = true
    this.paint()
  }

  pause() {
    if (!this.isStarted) return
    if (this.isPaused) return
    this.isPaused = true
    cancelAnimationFrame(this.animationId)
  }

  resume() {
    if (!this.isStarted) return
    if (!this.isPaused) return
    this.isPaused = false
    this.paint()
  }

  stop() {
    if (!this.isStarted) return
    this.isStarted = false
    cancelAnimationFrame(this.animationId)
  }

  paint() {
    const t1 = Date.now()
    this.render(this.loadedScene)
    const t2 = Date.now()
    Time.deltaTime = (t2 - t1) / 1000
    this.animationId = requestAnimationFrame(this.paint.bind(this))
  }
}

export default Engine