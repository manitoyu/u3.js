import Vector2 from '../core/Vector2'

type Axes = {
  MouseX: number
  MouseY: number
  MouseScrollWheel: number
}

class Input {
  private static mouseButtonIsDown: number
  private canvas: HTMLCanvasElement
  private static mouseDownPosition: Vector2 = new Vector2(0, 0)
  private static mousePosition: Vector2 = new Vector2(0, 0)

  private static axes: Axes = {
    MouseX: 0,
    MouseY: 0,
    MouseScrollWheel: 0
  }

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas

    this.canvas.addEventListener('mousedown', e => {
      Input.mouseButtonIsDown = e.button
      Input.mouseDownPosition.x = e.offsetX
      Input.mouseDownPosition.y = e.offsetY
    })

    this.canvas.addEventListener('mousemove', e => {
      if (Input.mouseButtonIsDown == -1) return
      Input.mousePosition.x = e.offsetX
      Input.mousePosition.y = e.offsetY
      Input.axes.MouseX = (Input.mousePosition.x - Input.mouseDownPosition.x) / this.canvas.width
      Input.axes.MouseY = (Input.mousePosition.y - Input.mouseDownPosition.y) / this.canvas.height
    })

    this.canvas.addEventListener('mouseup', e => {
      Input.mouseButtonIsDown = -1

      Input.mouseDownPosition = new Vector2(0, 0)
      Input.mousePosition = new Vector2(0, 0)
      Input.axes.MouseX = 0
      Input.axes.MouseY = 0
    })

    this.canvas.addEventListener('wheel', e => {
      Input.axes.MouseScrollWheel = e.deltaY / 500
      setTimeout(() => {
        Input.axes.MouseScrollWheel = 0
      }, 250)
    })
  }

  static getAxisRaw(axisName: keyof Axes): number {
    return Input.axes[axisName]
  }

  static getMouseButton(button: number): boolean {
    return button == Input.mouseButtonIsDown
  }
}

export default Input