import Transform from '../components/Transform'
import MeshFilter from '../components/MeshFilter'
import MeshRenderer from '../components/MeshRenderer'
import Vector2 from '../core/Vector2'
import Vector3 from '../core/Vector3'
import { Triangle } from '../core/Structs'
import Script from '../components/Script'

class GameObject {
  transform: Transform = new Transform()
  meshFilter: MeshFilter
  meshRenderer: MeshRenderer
  scripts: Script[] = []

  addScript(script: Script) {
    this.scripts.push(script)
    script.transform = this.transform
    script.start()
  }

  submit(): Triangle[] {
    if (this.meshRenderer == undefined) return []

    const triangles: Triangle[] = []

    const localToWorldMatrix = this.transform.localToWorldMatrix
    
    for (let i = 0; i < this.meshFilter.mesh.triangles.length; i += 3) {
      const av = localToWorldMatrix.multiplyPoint(this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i]])
      const bv = localToWorldMatrix.multiplyPoint(this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i + 1]])
      const cv = localToWorldMatrix.multiplyPoint(this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i + 2]])
      const normal = bv.subtract(av).cross(cv.subtract(bv)).normalize()

      triangles.push({
        a: {
          position: this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i]],
          normal,
          texcoord: this.meshFilter.mesh.uv[this.meshFilter.mesh.triangles[i]]
        },
        b: {
          position: this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i + 1]],
          normal,
          texcoord: this.meshFilter.mesh.uv[this.meshFilter.mesh.triangles[i + 1]]
        },
        c: {
          position: this.meshFilter.mesh.vertices[this.meshFilter.mesh.triangles[i + 2]],
          normal,
          texcoord: this.meshFilter.mesh.uv[this.meshFilter.mesh.triangles[i + 2]]
        }
      })
    }

    return triangles
  }
}

export default GameObject