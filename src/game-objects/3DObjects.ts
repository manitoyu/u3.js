import GameObject from './GameObject'
import MeshFilter from '../components/MeshFilter'
import MeshRenderer from '../components/MeshRenderer'
import { cube, quad, sphere } from '../components/Mesh'

class Cube extends GameObject {
  constructor() {
    super()

    this.meshFilter = new MeshFilter()

    this.meshFilter.gameObject = this
    this.meshFilter.mesh = cube

    this.meshRenderer = new MeshRenderer()
    this.meshRenderer.gameObject = this
  }
}

class Quad extends GameObject {
  constructor() {
    super()

    this.meshFilter = new MeshFilter()
    this.meshFilter.gameObject = this
    this.meshFilter.mesh = quad

    this.meshRenderer = new MeshRenderer()
    this.meshRenderer.gameObject = this
  }
}

class Sphere extends GameObject {
  constructor() {
    super()

    this.meshFilter = new MeshFilter()
    this.meshFilter.gameObject = this
    this.meshFilter.mesh = sphere

    this.meshRenderer = new MeshRenderer()
    this.meshRenderer.gameObject = this
  }
}

export {
  Cube,
  Sphere,
  Quad
}