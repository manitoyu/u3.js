import GameObject from './GameObject'
import Vector2 from '../core/Vector2'
import Vector3 from '../core/Vector3'
import Vector4 from '../core/Vector4'
import Matrix4x4 from '../core/Matrix4x4'
import Quaternion from '../core/Quaternion'
import assert from '../utils/assert'
import { clamp } from '../utils/utils'

class Camera extends GameObject {
  aspect: number = 0
  farClipPlane: number = 0
  nearClipPlane: number = 0
  pixelWidth: number = 0
  pixelHeight: number = 0
  projectionMatrix: Matrix4x4
  worldToCameraMatrix: Matrix4x4
  worldToViewportMatrix: Matrix4x4
  viewportToWorldMatrix: Matrix4x4
  viewportToScreenMatrix: Matrix4x4
  screenToViewportMatrix: Matrix4x4
  fieldOfView: number = 60

  static readonly main = new Camera()

  render() {
    const n = new Vector3(0, 0, -1).normalize()
    const v = new Vector3(0, 1, 0)
    const u = n.cross(v)

    this.worldToCameraMatrix = 
      new Matrix4x4(
        u.x, u.y, u.z, 0,
        v.x, v.y, v.z, 0,
        n.x, n.y, n.z, 0,
        0, 0, 0, 1)
      .multiplyMatrix(Matrix4x4.fromQuaternion(this.transform.rotation))
      .multiplyMatrix(Matrix4x4.TRS(this.transform.position.multiply(-1), Quaternion.identity, Vector3.one))

    this.projectionMatrix = Matrix4x4.perspective(this.fieldOfView, this.aspect, this.nearClipPlane, this.farClipPlane)

    this.worldToViewportMatrix = this.projectionMatrix
      .multiplyMatrix(this.worldToCameraMatrix)
 
    this.viewportToScreenMatrix = new Matrix4x4(
      this.pixelWidth / 2, 0, 0, this.transform.position.x,
      0, this.pixelHeight / 2, 0, this.transform.position.y,
      0, 0, 1, .5,
      0, 0, 0, 1
    )

    this.screenToViewportMatrix = this.viewportToScreenMatrix.inverse()

    this.viewportToWorldMatrix = this.worldToViewportMatrix.inverse()
  }

  screenToViewPoint(position: Vector3): Vector3 {
    return this.screenToViewportMatrix
      .multiplyPoint(new Vector3(position.x - this.pixelWidth / 2, position.y - this.pixelHeight / 2, position.z))
  }

  screenToWorldPoint(position: Vector3): Vector3 {
    return this.viewportToWorldPoint(this.screenToViewPoint(position))
  }

  viewportToWorldPoint(position: Vector3): Vector3 {
    const vec = this.viewportToWorldMatrix
      .multiplyVector4(new Vector4(position.x * position.z, position.y * position.z, -this.projectionMatrix.m33 * position.z + this.projectionMatrix.m34, position.z))

    return new Vector3(vec.x, vec.y, vec.z)
  }

  worldToClipSpaceCoordinate(position: Vector3): Vector4 {
    return this.worldToViewportMatrix
      .multiplyVector4(new Vector4(position.x, position.y, position.z, 1))
  }

  clipSpaceToWorldPoint(coordinate: Vector4): Vector3 {
    const vec = this.viewportToWorldMatrix
      .multiplyVector4(coordinate)
    
    return new Vector3(vec.x, vec.y, vec.z)
  }

  worldToViewportPoint(position: Vector3): Vector3 {
    const clipVec = this.worldToClipSpaceCoordinate(position)

    return new Vector3(clipVec.x / clipVec.w, clipVec.y / clipVec.w, clipVec.z)
  }

  viewportToScreenPoint(position: Vector3): Vector3 {
    const screenVec = this.viewportToScreenMatrix.multiplyPoint(position)

    return screenVec.set(
      Math.round(this.pixelWidth / 2 + screenVec.x),
      Math.round(this.pixelHeight / 2 + screenVec.y),
      screenVec.z,
    )
  }

  worldToScreenPoint(position: Vector3): Vector3 {
    assert(this.worldToCameraMatrix !== undefined, 'worldToCameraMatrix is undefined')

    return this.viewportToScreenPoint(this.worldToViewportPoint(position))
  }
}

export default Camera