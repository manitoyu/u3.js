import Tramsform from '../components/Transform'
import Color from '../core/Color'
import Vector3 from '../core/Vector3'

class Light {
  transform: Tramsform = new Tramsform()
  intensity: number = 1
  color: Color = new Color(1, 1, 1, 1)

  static main = new Light()

  get lightDirection() {
    return this.transform.localToWorldMatrix.multiplyPoint(Vector3.zero).normalize()
  }
}

export default Light