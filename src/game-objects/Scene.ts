import GameObject from './GameObject'

class Scene {
  gameObjects: GameObject[] = []

  addGameObject(gameObject: GameObject) {
    this.gameObjects.push(gameObject)
  }

  removeGameObject(gameObject: GameObject) {
    this.gameObjects = this.gameObjects.filter(g => g != gameObject)
  }

  clearAllGameObjects() {
    this.gameObjects = []
  }
}

export default Scene