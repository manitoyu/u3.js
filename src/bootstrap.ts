import Camera from './game-objects/Camera'
import Quaternion from './core/Quaternion'
import Vector3 from './core/Vector3'
import Light from './game-objects/Light'

Camera.main.fieldOfView = 60
Camera.main.pixelWidth = 800
Camera.main.pixelHeight = 600
Camera.main.aspect = Camera.main.pixelWidth / Camera.main.pixelHeight
Camera.main.nearClipPlane = .3
Camera.main.farClipPlane = 1000
Camera.main.transform.rotation = Quaternion.euler(0, 0, 0)
Camera.main.transform.position = new Vector3(0, 0, -6)
Camera.main.render()

Light.main.transform.position = new Vector3(0, 1, -1)
Light.main.intensity = 1
