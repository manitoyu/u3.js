export function flatMap<A, B>(arr: A[], f: (a: A) => B[]) {
  return arr.reduce((b, a) => b.concat(f(a)), [])
}