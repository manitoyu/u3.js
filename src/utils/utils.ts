const { abs } = Math

export function equalf(a: number, b: number) {
  return abs(a - b) <= .01
}

export function clamp(number: number, lower: number, upper: number) {
  if (number < lower) return lower
  if (number > upper) return upper
  return number
}

export function lerp(a: number, b: number, t: number) {
  return a + (b - a) * t
}

export function range(start: number, end: number): number[] {
  const count = start <= end ? end - start : start - end
  return Array(count).fill(0).map((_, index) => index)
}

export function chunk<T>(array: T[], size: number): T[][] {
  const result: T[][] = []
  const len = Math.ceil(array.length / size)

  for (let i = 0; i < len; i ++) {
    result.push(array.slice(i * size, (i + 1) * size))
  }

  return result
}