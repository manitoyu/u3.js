import { equalf } from '../utils/utils'

class Vector4 {
  constructor(
    public x: number,
    public y: number,
    public z: number,
    public w: number) {
  }

  set(x: number, y: number, z: number, w: number) {
    this.x = x
    this.y = y
    this.z = z
    this.w = w

    return this
  }

  equal(v: Vector4): boolean {
    return equalf(this.x, v.x) && equalf(this.y, v.y) && equalf(this.z, v.z) && equalf(this.w, v.w)
  }

  static lerp(a: Vector4, b: Vector4, t: number) {
    return b.subtract(a).multiply(t).add(a)
  }

  subtract(v: Vector4): Vector4 {
    return new Vector4(this.x - v.x, this.y - v.y, this.z - v.z, this.w - v.w)
  }
  
  multiply(n: number): Vector4 {
    return new Vector4(this.x * n, this.y * n, this.z * n, this.w * n)
  }

  add(v: Vector4) {
    return new Vector4(this.x + v.x, this.y + v.y, this.z + v.z, this.w + v.w)
  }
}

export default Vector4