class Color {
  constructor(public r: number, public g: number, public b: number, public a: number) {
  }

  set(r: number, g: number, b: number, a: number) {
    this.r = r
    this.g = g
    this.b = b
    this.a = a

    return this
  }
}

export default Color