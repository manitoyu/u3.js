import Vector3 from './Vector3'
import { Vertex } from './Structs'
const { abs } = Math

// 画线
export function Bresenham(x0: number, y0: number, x1: number, y1: number, cb: (x: number, y: number, t: number) => void) {
  const dx = abs(x1 - x0)
  const dy = abs(y1 - y0)
  let p = 2 * dy - dx
  let x: number
  let y: number

  if (dx == 0) {
    if (y0 < y1) {
      y = y0
    } else {
      y = y1
      y1 = y0
      y0 = y
    }

    while (y < y1) {
      cb(x0, y, (y - y0) / (y1 - y0))
      y ++
    }
    return
  }

  if (dy == 0) {
    if (x0 < x1) {
      x = x0
    } else {
      x = x1
      x1 = x0
      x0 = x
    }

    while (x < x1) {
      cb(x, y0, (x - x0) / (x1 - x0))
      x ++
    }
    return
  }

  if (x0 > x1) {
    x = x1
    y = y1
    x1 = x0
    y1 = y0
    x0 = x
    y0 = y
  } else {
    x = x0
    y = y0
  }

  while (x < x1) {
    cb(x, y, (x - x0) / (x1 - x0))
    x ++
    if (p < 0) {
      p += 2 * dy
    } else {
      if (y0 < y1) {
        y ++
      } else {
        y --
      }
      p += 2 * (dy - dx)
    }
  }
}

// 三角形扫描
export function depthScan(A: Vector3, B: Vector3, C: Vector3, cb: (x: number, y: number, z: number) => void) {
  const vertices = [A, B, C]
  vertices.sort((a, b) => a.y < b.y ? 0 : 1)

  A = vertices[0]
  B = vertices[1]
  C = vertices[2]

  let kAB = (B.y - A.y) / (B.x - A.x)
  if (isNaN(kAB)) kAB = 0

  let kBC = (C.y - B.y) / (C.x - B.x)
  if (isNaN(kBC)) kBC = 0
  
  let kCA = (A.y - C.y) / (A.x - C.x)
  if (isNaN(kCA)) kCA = 0

  const AB = B.subtract(A)
  const BC = C.subtract(B)
  const n = AB.cross(BC).normalize()
  const d = A.dot(n)
  
  const z = (x: number, y: number) => -(n.x * x + n.y * y - d) / n.z 

  for (let i = 0; i < B.y - A.y; i ++) {
    Bresenham(Math.round(1 / kAB * i + A.x), A.y + i, Math.round(1 / kCA * i + A.x), A.y + i, (x, y) => {
      cb(x, y, z(x, y))
    })
  }

  for (let i = C.y - B.y; i > 0; i --) {
    Bresenham(Math.round(C.x - 1 / kBC * i), C.y - i, Math.round(C.x - 1 / kCA * i), C.y - i, (x, y) => {
      cb(x, y, z(x, y))
    })
  }
}

export function Euclidean(a: number, b: number) {
  let c: number
  
  while (b != 0) {
    c = a % b
    a = b
    b = c
  }

  return a
}
