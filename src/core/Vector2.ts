class Vector2 {
  static readonly up: Vector2 = new Vector2(0, 1)
  static readonly down: Vector2 = new Vector2(0, -1)
  static readonly left: Vector2 = new Vector2(-1, 0)
  static readonly right: Vector2 = new Vector2(1, 0)
  static readonly one: Vector2 = new Vector2(1, 1)
  static readonly zero: Vector2 = new Vector2(0, 0)

  constructor(public x: number, public y: number) {
  }

  set(x: number, y: number) {
    this.x = x
    this.y = y

    return this
  }

  static lerp(a: Vector2, b: Vector2, t: number) {
    return b.subtract(a).multiply(t).add(a)
  }

  subtract(v: Vector2): Vector2 {
    return new Vector2(this.x - v.x, this.y - v.y)
  }
  
  multiply(n: number): Vector2 {
    return new Vector2(this.x * n, this.y * n)
  }

  add(v: Vector2) {
    return new Vector2(this.x + v.x, this.y + v.y)
  }
}

export default Vector2