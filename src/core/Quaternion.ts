import Vector3 from './Vector3'
const { cos, sin, abs, atan2, PI, asin } = Math

class Quaternion {
  static readonly identity: Quaternion = new Quaternion(0, 0, 0, 1)

  constructor(
    public x: number,
    public y: number,
    public z: number,
    public w: number) {
  }

  // static angle(a: Quaternion, b: Quaternion): number {

  // }

  // static angleAxis(angle: number, axis: Vector3): Quaternion {

  // }

  get eulerAngles(): Vector3 {
    const sp = -2 * (this.y * this.z - this.w * this.x)

    if (abs(sp) > .9999) {
      return new Vector3(
        1.570796 * sp / PI * 180,
        atan2(-this.x * this.z - this.w * this.y, .5 - this.y ** 2 - this.z ** 2) / PI * 180,
        0
      )
    } else {
      return new Vector3(
        asin(sp) / PI * 180,
        atan2(this.x * this.z + this.w * this.y, .5 - this.x ** 2 - this.y ** 2) / PI * 180,
        atan2(this.x * this.y + this.w * this.z, .5 - this.x ** 2 - this.z ** 2) / PI * 180,
      )
    }
  }

  dot(a: Quaternion): number {
    return this.w * a.w + this.x * a.x + this.y * a.y + this.z * a.z
  }

  cross(a: Quaternion): Quaternion {
    return new Quaternion(
      this.w * a.x + this.x * a.w + this.y * a.z - this.z * a.y,
      this.w * a.y + this.y * a.w + this.z * a.x - this.x * a.z,
      this.w * a.z + this.z * a.w + this.x * a.y - this.y * a.x,
      this.w * a.w - this.x * a.x - this.y * a.y - this.z * a.z
    )
  }

  inverse(): Quaternion {
    return new Quaternion(-this.x, -this.y, -this.z, this.w)
  }

  static euler(x: number, y: number, z: number): Quaternion {
    const xRad = x / 2 / 180 * PI 
    const yRad = y / 2 / 180 * PI
    const zRad = z / 2 / 180 * PI

    return new Quaternion(
      cos(yRad) * sin(xRad) * cos(zRad) + sin(yRad) * cos(xRad) * sin(zRad),
      sin(yRad) * cos(xRad) * cos(zRad) - cos(yRad) * sin(xRad) * sin(zRad),
      cos(yRad) * cos(xRad) * sin(zRad) - sin(yRad) * sin(xRad) * cos(zRad),
      cos(yRad) * cos(xRad) * cos(zRad) + sin(yRad) * sin(xRad) * sin(zRad)
    )
  }

  // static lerp(a: Quaternion, b: Quaternion, t: number): Quaternion {

  // }

  // static slerp(a: Quaternion, b: Quaternion, t: number): Quaternion {

  // }
}

export default Quaternion