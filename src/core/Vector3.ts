import assert from '../utils/assert'
import { equalf, clamp } from '../utils/utils'

const { acos } = Math

class Vector3 {
  static readonly back: Vector3 = new Vector3(0, 0, -1)
  static readonly down: Vector3 = new Vector3(0, -1, 0)
  static readonly forward: Vector3 = new Vector3(0, 0, 1)
  static readonly left: Vector3 = new Vector3(-1, 0, 0)
  static readonly one: Vector3 = new Vector3(1, 1, 1)
  static readonly right: Vector3 = new Vector3(1, 0, 0)
  static readonly up: Vector3 = new Vector3(0, 1, 0)
  static readonly zero: Vector3 = new Vector3(0, 0, 0)

  x: number = 0
  y: number = 0
  z: number = 0

  constructor(x: number, y: number, z: number) {
    this.x = x
    this.y = y
    this.z = z
  }

  set(x: number, y: number, z: number) {
    this.x = x
    this.y = y
    this.z = z

    return this
  }

  get magnitude(): number {
    return Math.sqrt(this.x ** 2 + this.y ** 2 + this.z ** 2)
  }

  get normalized(): Vector3 {
    return new Vector3(this.x, this.y, this.z).normalize()
  }

  static lerp(a: Vector3, b: Vector3, t: number): Vector3 {
    return b.subtract(a).multiply(t).add(a)
  }

  angle(to: Vector3): number {
    const deno = this.magnitude * to.magnitude
    
    if (deno == 0) return 0
    return acos(clamp(this.dot(to) / deno, -1, 1)) / Math.PI * 180
  }

  equal(v: Vector3): boolean {
    return equalf(this.x, v.x) && equalf(this.y, v.y) && equalf(this.z, v.z)
  }

  add(v: Vector3): Vector3 {
    return new Vector3(this.x + v.x, this.y + v.y, this.z + v.z)
  }

  subtract(v: Vector3): Vector3 {
    return new Vector3(this.x - v.x, this.y - v.y, this.z - v.z)
  }

  multiply(n: number): Vector3 {
    return new Vector3(this.x * n, this.y * n, this.z * n)
  }

  divide(n: number): Vector3 {
    assert(n != 0, 'Vector3 divide n must not be 0')
    const oneOverN = 1 / n
    return new Vector3(this.x * oneOverN, this.y * oneOverN, this.z * oneOverN)
  }

  addSelf(v: Vector3): Vector3 {
    this.x += v.x
    this.y += v.y
    this.z += v.z
    return this
  }

  subtractSelf(v: Vector3): Vector3 {
    this.x -= v.x
    this.y -= v.y
    this.z -= v.z
    return this
  }

  multiplySelf(n: number): Vector3 {
    this.x *= n
    this.y *= n
    this.z *= n
    return this
  }

  divideSelf(n: number): Vector3 {
    assert(n != 0, 'Vector3 divide n must not be 0')
    const oneOverN = 1 / n
    this.x *= oneOverN
    this.y *= oneOverN
    this.z *= oneOverN
    return this
  }

  // FIXME 改变了自身
  oppsite(): Vector3 {
    return this.multiply(-1)
  }

  normalize(): Vector3 {
    const magnitude = this.magnitude

    if (magnitude == 0) return this
    
    const oneOverMag = 1 / magnitude
    this.x *= oneOverMag
    this.y *= oneOverMag
    this.z *= oneOverMag
    return this
  }

  dot(v: Vector3): number {
    return this.x * v.x + this.y * v.y + this.z * v.z
  }

  cross(v: Vector3): Vector3 {
    return new Vector3(
      this.y * v.z - this.z * v.y,
      this.z * v.x - this.x * v.z,
      this.x * v.y - this.y * v.x
    )
  }

  distance(v: Vector3): number {
    const dx = this.x - v.x
    const dy = this.y - v.y
    const dz = this.z - v.z
    return Math.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
  }

  scale(v: Vector3): Vector3 {
    return new Vector3(this.x * v.x, this.y * v.y, this.z * v.z)
  }

  zero(): Vector3 {
    this.x = 0
    this.y = 0
    this.z = 0
    return this
  }
}

export default Vector3