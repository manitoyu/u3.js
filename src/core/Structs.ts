import Matrix4x4 from './Matrix4x4'
import Color from './Color'
import Vector2 from './Vector2'
import Vector3 from './Vector3'
import Vector4 from './Vector4'

interface Uniform {
  MVPMatrix?: Matrix4x4
  MVMatrix?: Matrix4x4
}

interface VertexIn {
  color?: Color
  normal?: Vector3
  position?: Vector4
  texcoord?: Vector2
}

interface VertexOut {
  color?: Color
  normal?: Vector3
  position?: Vector4
  texcoord?: Vector2
  mvPosition?: Vector4
}

interface Vertex {
  position: Vector3
  normal: Vector3
  texcoord: Vector2
}

interface Triangle {
  a: Vertex
  b: Vertex
  c: Vertex
}

export {
  Vertex,
  Triangle,
  Uniform,
  VertexIn,
  VertexOut
}