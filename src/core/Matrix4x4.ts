import Vector3 from '../core/Vector3'
import Vector4 from '../core/Vector4'
import Quaternion from '../core/Quaternion'
import assert from '../utils/assert'

const { tan, PI } = Math

class Matrix4x4 {
  constructor(
    public m11: number, public m12: number, public m13: number, public m14: number,
    public m21: number, public m22: number, public m23: number, public m24: number,
    public m31: number, public m32: number, public m33: number, public m34: number,
    public m41: number, public m42: number, public m43: number, public m44: number) {
  }

  static identity = new Matrix4x4(
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1
  )

  static zero = new Matrix4x4(
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0
  )

  get transpose() {
    return new Matrix4x4(
      this.m11, this.m21, this.m31, this.m41,
      this.m12, this.m22, this.m32, this.m42,
      this.m13, this.m23, this.m33, this.m43,
      this.m14, this.m24, this.m34, this.m44
    )
  }

  multiplyNumber(n: number): Matrix4x4 {
    return new Matrix4x4(
      this.m11 * n, this.m12 * n, this.m13 * n, this.m14 * n,
      this.m21 * n, this.m22 * n, this.m23 * n, this.m24 * n,
      this.m31 * n, this.m32 * n, this.m33 * n, this.m34 * n,
      this.m41 * n, this.m42 * n, this.m43 * n, this.m44 * n
    )
  }

  multiplyMatrix(rhs: Matrix4x4): Matrix4x4 {
    return new Matrix4x4(
      this.m11 * rhs.m11 + this.m12 * rhs.m21 + this.m13 * rhs.m31 + this.m14 * rhs.m41,
      this.m11 * rhs.m12 + this.m12 * rhs.m22 + this.m13 * rhs.m32 + this.m14 * rhs.m42,
      this.m11 * rhs.m13 + this.m12 * rhs.m23 + this.m13 * rhs.m33 + this.m14 * rhs.m43,
      this.m11 * rhs.m14 + this.m12 * rhs.m24 + this.m13 * rhs.m34 + this.m14 * rhs.m44,

      this.m21 * rhs.m11 + this.m22 * rhs.m21 + this.m23 * rhs.m31 + this.m24 * rhs.m41,
      this.m21 * rhs.m12 + this.m22 * rhs.m22 + this.m23 * rhs.m32 + this.m24 * rhs.m42,
      this.m21 * rhs.m13 + this.m22 * rhs.m23 + this.m23 * rhs.m33 + this.m24 * rhs.m43,
      this.m21 * rhs.m14 + this.m22 * rhs.m24 + this.m23 * rhs.m34 + this.m24 * rhs.m44,

      this.m31 * rhs.m11 + this.m32 * rhs.m21 + this.m33 * rhs.m31 + this.m34 * rhs.m41,
      this.m31 * rhs.m12 + this.m32 * rhs.m22 + this.m33 * rhs.m32 + this.m34 * rhs.m42,
      this.m31 * rhs.m13 + this.m32 * rhs.m23 + this.m33 * rhs.m33 + this.m34 * rhs.m43,
      this.m31 * rhs.m14 + this.m32 * rhs.m24 + this.m33 * rhs.m34 + this.m34 * rhs.m44,

      this.m41 * rhs.m11 + this.m42 * rhs.m21 + this.m43 * rhs.m31 + this.m44 * rhs.m41,
      this.m41 * rhs.m12 + this.m42 * rhs.m22 + this.m43 * rhs.m32 + this.m44 * rhs.m42,
      this.m41 * rhs.m13 + this.m42 * rhs.m23 + this.m43 * rhs.m33 + this.m44 * rhs.m43,
      this.m41 * rhs.m14 + this.m42 * rhs.m24 + this.m43 * rhs.m34 + this.m44 * rhs.m44
    )
  }

  multiplyVector4(v: Vector4): Vector4 {
    return new Vector4(
      v.x * this.m11 + v.y * this.m12 + v.z * this.m13 + v.w * this.m14,
      v.x * this.m21 + v.y * this.m22 + v.z * this.m23 + v.w * this.m24,
      v.x * this.m31 + v.y * this.m32 + v.z * this.m33 + v.w * this.m34,
      v.x * this.m41 + v.y * this.m42 + v.z * this.m43 + v.w * this.m44
    )
  }

  multiplyPoint(v: Vector3): Vector3 {
    const result = this.multiplyVector4(new Vector4(v.x, v.y, v.z, 1))
    return new Vector3(result.x, result.y, result.z)
  }

  static TRS(pos: Vector3, q: Quaternion, s: Vector3): Matrix4x4 {
    const R = Matrix4x4.fromQuaternion(q)

    return new Matrix4x4(
      R.m11 * s.x, R.m21, R.m31, pos.x,
      R.m12, R.m22 * s.y, R.m32, pos.y,
      R.m13, R.m23, R.m33 * s.z, pos.z,
      0, 0, 0, 1
    )
  }

  static fromQuaternion(q: Quaternion): Matrix4x4 {
    return new Matrix4x4(
      1 - 2 * q.y ** 2 - 2 * q.z ** 2,
      2 * q.x * q.y + 2 * q.w * q.z,
      2 * q.x * q.z - 2 * q.w * q.y,
      0,

      2 * q.x * q.y - 2 * q.w * q.z,
      1 - 2 * q.x ** 2 - 2 * q.z ** 2,
      2 * q.y * q.z + 2 * q.w * q.x,
      0,

      2 * q.x * q.z + 2 * q.w * q.y,
      2 * q.y * q.z - 2 * q.w * q.x,
      1 - 2 * q.x ** 2 - 2 * q.y ** 2,
      0,

      0, 0, 0, 1
    )
  }

  static perspective(fov: number, aspect: number, zNear: number, zFar: number) {
    const zoom = 1 / tan(fov / 2 / 180 * PI)

    return new Matrix4x4(
      zoom / aspect, 0, 0, 0,
      0, zoom, 0, 0,
      0, 0, (zNear + zFar) / (zNear - zFar), 2 * zNear * zFar / (zNear - zFar),
      0, 0, -1, 0
    )
  }

  inverse() {
    const det = 
      this.m11 * (
        + this.m22 * this.m33 * this.m44
        + this.m23 * this.m34 * this.m42
        + this.m24 * this.m32 * this.m43
        - this.m24 * this.m33 * this.m42
        - this.m23 * this.m32 * this.m44
        - this.m22 * this.m34 * this.m43
      ) + this.m12 * (
        + this.m21 * this.m33 * this.m44
        + this.m23 * this.m34 * this.m41
        + this.m24 * this.m31 * this.m43
        - this.m24 * this.m33 * this.m41
        - this.m23 * this.m31 * this.m44
        - this.m21 * this.m34 * this.m43
      ) + this.m13 * (
        + this.m21 * this.m32 * this.m44
        + this.m22 * this.m34 * this.m41
        + this.m24 * this.m31 * this.m42
        - this.m24 * this.m32 * this.m41
        - this.m22 * this.m31 * this.m44
        - this.m21 * this.m34 * this.m42
      ) + this.m14 * (
        + this.m21 * this.m32 * this.m43
        + this.m22 * this.m33 * this.m41
        + this.m23 * this.m31 * this.m42
        - this.m23 * this.m32 * this.m41
        - this.m22 * this.m31 * this.m43
        - this.m21 * this.m33 * this.m42
      )

    assert(det != 0, 'the matrix can not be inversed')

    const cof = new Matrix4x4(
      // 1
      + this.m22 * this.m33 * this.m44
      + this.m23 * this.m34 * this.m42
      + this.m24 * this.m32 * this.m43
      - this.m24 * this.m33 * this.m42
      - this.m23 * this.m32 * this.m44
      - this.m22 * this.m34 * this.m43,

      (+ this.m21 * this.m33 * this.m44
      + this.m23 * this.m34 * this.m41
      + this.m24 * this.m31 * this.m43
      - this.m24 * this.m33 * this.m41
      - this.m23 * this.m31 * this.m44
      - this.m21 * this.m34 * this.m43) * -1,

      + this.m21 * this.m32 * this.m44
      + this.m22 * this.m34 * this.m41
      + this.m24 * this.m31 * this.m42
      - this.m24 * this.m32 * this.m41
      - this.m22 * this.m31 * this.m44
      - this.m21 * this.m34 * this.m42,

      (+ this.m21 * this.m32 * this.m43
      + this.m22 * this.m33 * this.m41
      + this.m23 * this.m31 * this.m42
      - this.m23 * this.m32 * this.m41
      - this.m22 * this.m31 * this.m43
      - this.m21 * this.m33 * this.m42) * -1,

      // 2
      (+ this.m12 * this.m33 * this.m44
      + this.m13 * this.m34 * this.m42
      + this.m14 * this.m32 * this.m43
      - this.m14 * this.m33 * this.m42
      - this.m13 * this.m32 * this.m44
      - this.m12 * this.m34 * this.m43) * -1,

      + this.m11 * this.m33 * this.m44
      + this.m13 * this.m34 * this.m41
      + this.m14 * this.m31 * this.m43
      - this.m14 * this.m33 * this.m41
      - this.m13 * this.m31 * this.m44
      - this.m11 * this.m34 * this.m43,

      (+ this.m11 * this.m32 * this.m44
      + this.m12 * this.m34 * this.m41
      + this.m14 * this.m31 * this.m42
      - this.m14 * this.m32 * this.m41
      - this.m12 * this.m21 * this.m34
      - this.m11 * this.m34 * this.m42) * -1,

      + this.m11 * this.m32 * this.m43
      + this.m12 * this.m33 * this.m41
      + this.m13 * this.m31 * this.m42
      - this.m13 * this.m32 * this.m41
      - this.m12 * this.m31 * this.m43
      - this.m11 * this.m33 * this.m42,

      // 3
      + this.m12 * this.m23 * this.m44
      + this.m13 * this.m24 * this.m42
      + this.m14 * this.m22 * this.m43
      - this.m14 * this.m23 * this.m42
      - this.m13 * this.m22 * this.m44
      - this.m12 * this.m24 * this.m43,

      (+ this.m11 * this.m23 * this.m44
      + this.m13 * this.m24 * this.m41
      + this.m14 * this.m21 * this.m43
      - this.m14 * this.m23 * this.m41
      - this.m13 * this.m21 * this.m44
      - this.m11 * this.m24 * this.m43) * -1,

      + this.m11 * this.m22 * this.m44
      + this.m12 * this.m24 * this.m41
      * this.m14 * this.m21 * this.m42
      - this.m14 * this.m22 * this.m41
      - this.m12 * this.m21 * this.m44
      - this.m11 * this.m24 * this.m42,

      (+ this.m11 * this.m22 * this.m43
      + this.m12 * this.m23 * this.m41
      + this.m13 * this.m21 * this.m42
      - this.m13 * this.m22 * this.m41
      - this.m12 * this.m21 * this.m43
      - this.m11 * this.m23 * this.m42) * -1,

      // 4
      (+ this.m12 * this.m23 * this.m34
      + this.m13 * this.m24 * this.m32
      + this.m14 * this.m22 * this.m33
      - this.m14 * this.m23 * this.m32
      - this.m13 * this.m22 * this.m34
      - this.m12 * this.m24 * this.m33) * -1,

      + this.m11 * this.m23 * this.m34
      + this.m13 * this.m24 * this.m31
      + this.m14 * this.m21 * this.m33
      - this.m14 * this.m23 * this.m31
      - this.m13 * this.m21 * this.m34
      - this.m11 * this.m24 * this.m33,

      (+ this.m11 * this.m22 * this.m34 
      + this.m12 * this.m24 * this.m31 
      + this.m14 * this.m21 * this.m32
      - this.m14 * this.m22 * this.m31 
      - this.m12 * this.m21 * this.m34 
      - this.m11 * this.m24 * this.m32) * -1,

      + this.m11 * this.m22 * this.m33
      + this.m12 * this.m23 * this.m31
      + this.m13 * this.m21 * this.m32
      - this.m13 * this.m22 * this.m31
      - this.m12 * this.m21 * this.m33
      - this.m11 * this.m23 * this.m32
    ).transpose

    return cof.multiplyNumber(1 / det)
  }
}

export default Matrix4x4