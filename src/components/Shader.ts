import Matrix4x4 from '../core/Matrix4x4'
import Vector2 from '../core/Vector2'
import Vector3 from '../core/Vector3'
import Color from '../core/Color'
import Camera from '../game-objects/Camera'
import { Uniform, VertexIn, VertexOut, Triangle } from '../core/Structs'
import Light from '../game-objects/Light'

const { max } = Math

let texture: Uint8ClampedArray = undefined
const canvas = document.createElement('canvas')
const ctx = canvas.getContext('2d')
const image = new Image()
image.src = '/earth.jpg'
image.onload = () => {
  canvas.width = image.naturalWidth
  canvas.height = image.naturalHeight
  ctx.drawImage(image, 0, 0, image.naturalWidth, image.naturalHeight)
  texture = ctx.getImageData(0, 0, image.naturalWidth, image.naturalHeight).data
}

const color = new Color(0, 0, 0, 0)
const lightDirection = Light.main.lightDirection
const halfVector = lightDirection.add(Camera.main.transform.localToWorldMatrix.multiplyPoint(new Vector3(0, 0, -1))).normalize()
const ambient = new Vector3(.1, .1, .1)

class Shader {
  uniform: Uniform

  static main = new Shader()

  vertexProgram(input: VertexIn): VertexOut {
    return {
      position: this.uniform.MVPMatrix.multiplyVector4(input.position),
      mvPosition: this.uniform.MVMatrix.multiplyVector4(input.position),
      texcoord: input.texcoord,
      normal: input.normal
    }
  }

  surfaceProgram(output: VertexOut): Color {
    const diffuse = max(0, output.normal.dot(lightDirection))
    let specular = max(0, output.normal.dot(halfVector))

    specular = diffuse == 0 ? 0 : specular ** .5

    const index = (Math.round((1 - output.texcoord.y) * (image.naturalHeight - 1)) * image.naturalWidth
      + Math.round(output.texcoord.x * (image.naturalWidth - 1))) * 4
    
    const rgb = new Vector3(texture[index] / 255, texture[index + 1] / 255, texture[index + 2] / 255)
      .scale(ambient.add(Vector3.one.multiply(diffuse * Light.main.intensity)))
      .add(Vector3.one.multiply(specular * .2))

    return color.set(
      rgb.x,
      rgb.y,
      rgb.z,
      texture[index + 3] / 255
    )

    // return new Color((.5 + Math.abs(diffuse)) / 2, 0, 0, 1)
  }
}

export default Shader