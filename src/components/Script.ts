import Transform from './Transform'

class Script {
  transform: Transform

  start() {
  }

  update() {
  }

  lateUpdate() {
  }
}

export default Script