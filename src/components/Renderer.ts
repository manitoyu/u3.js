import Vector3 from '../core/Vector3'
import Quaternion from '../core/Quaternion'
import Matrix4x4 from '../core/Matrix4x4'
import { Cube, Sphere, Quad } from '../game-objects/3DObjects'
import Time from '../components/Time'
import Engine from '../engine/Engine'
import Scene from '../game-objects/Scene'
import Script from '../components/Script'
import { flatMap } from '../utils/funtional'
import GameObject from '../game-objects/GameObject'
import Camera from '../game-objects/Camera'
import Input from '../engine/Input'

const canvas = document.createElement('canvas')
canvas.width = 800
canvas.height = 600
canvas.style.display = 'block'
document.body.insertBefore(canvas, document.body.children[0])

const startBtn = document.createElement('button')
startBtn.textContent = 'start'
startBtn.addEventListener('click', () => engine.start())
document.body.insertBefore(startBtn, canvas)

const pauseBtn = document.createElement('button')
pauseBtn.textContent = 'pause'
pauseBtn.addEventListener('click', () => engine.pause())
document.body.insertBefore(pauseBtn, canvas)

const resumeBtn = document.createElement('button')
resumeBtn.textContent = 'resume'
resumeBtn.addEventListener('click', () => engine.resume())
document.body.insertBefore(resumeBtn, canvas)

// const stopBtn = document.createElement('button')
// stopBtn.textContent = 'stop'
// stopBtn.addEventListener('click', () => engine.stop())
// document.body.insertBefore(stopBtn, canvas)

const cubeBtn = document.createElement('button')
cubeBtn.textContent = 'cube'
cubeBtn.addEventListener('click', () => {
  scene.clearAllGameObjects()
  cubes.forEach(cube => scene.addGameObject(cube))
  scene.addGameObject(gameObject)
})
document.body.insertBefore(cubeBtn, canvas)

const earthBtn = document.createElement('button')
earthBtn.textContent = 'earch'
earthBtn.addEventListener('click', () => {
  scene.clearAllGameObjects()
  scene.addGameObject(sphere)
  scene.addGameObject(gameObject)
})
document.body.insertBefore(earthBtn, canvas)

const span = document.createElement('span')
document.body.insertBefore(span, canvas)

const engine = new Engine(canvas)
const scene = new Scene()
engine.loadScene(scene)

const level = [-1, 0, 1]

const cubes: Cube[] = flatMap<number, Cube>(level, x => {
  return flatMap<number, Cube>(level, y => {
    return flatMap<number, Cube>(level, z => {
      const cube = new Cube()

      cube.addScript(new class extends Script {
        start() {
          this.transform.translate(new Vector3(x, y, z))
        }
        update() {
          if (this.transform.position.y == 1 || this.transform.position.y == -1) {
            this.transform.rotateAround(Vector3.zero, Vector3.up, 45 * Time.deltaTime)
          }
          if (this.transform.position.y == 0) {
            this.transform.rotateAround(Vector3.zero, Vector3.down, 45 * Time.deltaTime)
          }
        }
      })

      return [cube]
    })
  })
})

// cubes.forEach(cube => scene.addGameObject(cube))

// const quad = new Quad()
// scene.addGameObject(quad)

const sphere = new Sphere()
scene.addGameObject(sphere)

sphere.addScript(new class extends Script {
  start() {
    // this.transform.translate(new Vector3(3, 0, 0))
  }

  update() {
    // this.transform.rotateAround(Vector3.zero, Vector3.up, 30 * Time.deltaTime)
    this.transform.rotate(new Vector3(0, 90 * Time.deltaTime, 0))
  }
})

const gameObject = new GameObject
gameObject.addScript(new class extends Script {
  x: number = 0
  y: number = 0
  z: number = 0

  lastTime: number = 0
  time: number = 0

  start() {
    const euler = Camera.main.transform.rotation.eulerAngles

    this.x = euler.x
    this.y = euler.y
    this.z = Camera.main.transform.position.z
  }

  update() {
    span.innerHTML = `FPS: ${Math.round(1 / Time.deltaTime)}`
  }

  lateUpdate() {
    if (Input.getMouseButton(0)) {
      this.y += Input.getAxisRaw('MouseX') * 30
      this.x += Input.getAxisRaw('MouseY') * 30

      Camera.main.transform.rotation = Quaternion.euler(this.x, this.y, 0)
      Camera.main.transform.position = Matrix4x4.fromQuaternion(Camera.main.transform.rotation.inverse()).multiplyPoint(Vector3.forward.multiply(this.z))
      Camera.main.render()
    }

    this.z -= Input.getAxisRaw('MouseScrollWheel')
    Camera.main.transform.position = Matrix4x4.fromQuaternion(Camera.main.transform.rotation.inverse()).multiplyPoint(Vector3.forward.multiply(this.z))
    Camera.main.render()
  }
})

scene.addGameObject(gameObject)

// function sameSide(A: Vector3, B: Vector3, C: Vector3, P: Vector3) {
//   const AB = B.subtract(A)
//   const AC = C.subtract(A)
//   const AP = P.subtract(A)

//   return AB.cross(AC).dot(AB.cross(AP)) >= 0
// }
