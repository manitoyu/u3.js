import Vector3 from '../core/Vector3'
import Quaternion from '../core/Quaternion'
import Component from './Component'
import Matrix4x4 from '../core/Matrix4x4'

const { cos, sin, PI } = Math

class Transform extends Component {
  position: Vector3 = Vector3.zero
  rotation: Quaternion = Quaternion.identity
  scale: Vector3 = Vector3.one

  rotate(eulerAngles: Vector3) {
    this.rotation = this.rotation.cross(
      Quaternion.euler(eulerAngles.x, eulerAngles.y, eulerAngles.z))
  }

  lookAt(target: Transform, worldUp: Vector3 = Vector3.up) {

  }

  rotateAround(point: Vector3, axis: Vector3, angle: number) {
    const n = axis.normalized

    const cv = cos(angle / 180 * PI)
    const sv = sin(angle / 180 * PI)

    this.position = new Matrix4x4(
      n.x ** 2 * (1 - cv) + cv,
      n.x * n.y * (1 - cv) + n.z * sv,
      n.x * n.z * (1 - cv) + n.y * sv,
      0,

      n.x * n.y * (1 - cv) + n.z * sv,
      n.y ** 2 * (1 - cv) + cv,
      n.y * n.z * (1 - cv) - n.x * sv,
      0,

      n.x * n.z * (1 - cv) - n.y * sv,
      n.y * n.z * (1 - cv) + n.x * sv,
      n.z ** 2 * (1 - cv) + cv,
      0,

      0, 0, 0, 1
    ).multiplyPoint(this.position.subtract(point)).add(point)

    this.rotation = this.rotation.cross(Quaternion.euler(n.x * angle, n.y * angle, n.z * angle))
  }

  translate(translation: Vector3) {
    this.position = Matrix4x4.TRS(translation, Quaternion.identity, Vector3.one)
      .multiplyPoint(this.position)
  }

  get localToWorldMatrix() {
    return Matrix4x4.TRS(
      this.position,
      this.rotation,
      this.scale
    )
  }

  get worldToLocalMatrix() {
    return this.localToWorldMatrix.inverse()
  }
}

export default Transform