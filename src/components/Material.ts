import Color from '../core/Color'

class Material {
  color: Color = new Color(0, 0, 0, 1)

  shininess: number = .5
}

export default Material