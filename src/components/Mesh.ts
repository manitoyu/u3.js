import Vector2 from '../core/Vector2'
import Vector3 from '../core/Vector3'
import { chunk, range, clamp } from '../utils/utils'

const { acos, sin, PI, asin } = Math

class Mesh {
  vertices: Vector3[]
  uv: Vector2[]
  uv2: Vector2[]
  triangles: number[]
}

class CubeMesh extends Mesh {
  vertices: Vector3[] = [
    new Vector3(-.5, .5, -.5),
    new Vector3(-.5, -.5, -.5),
    new Vector3(.5, .5, -.5),
    new Vector3(.5, -.5, -.5),

    new Vector3(.5, .5, -.5),
    new Vector3(.5, -.5, -.5),
    new Vector3(.5, .5, .5),
    new Vector3(.5, -.5, .5),

    new Vector3(.5, .5, .5),
    new Vector3(.5, -.5, .5),
    new Vector3(-.5, .5, .5),
    new Vector3(-.5, -.5, .5),

    new Vector3(-.5, .5, .5),
    new Vector3(-.5, -.5, .5),
    new Vector3(-.5, .5, -.5),
    new Vector3(-.5, -.5, -.5),

    new Vector3(-.5, .5, .5),
    new Vector3(-.5, .5, -.5),
    new Vector3(.5, .5, .5),
    new Vector3(.5, .5, -.5),

    new Vector3(-.5, -.5, -.5),
    new Vector3(-.5, -.5, .5),
    new Vector3(.5, -.5, -.5),
    new Vector3(.5, -.5, .5),
  ]

  uv: Vector2[] = [
    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0),

    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0),

    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0),
     
    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0),
     
    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0),

    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0)
  ]

  uv2: Vector2[] = [
    new Vector2(0, 0),
    new Vector2(0, .5),
    new Vector2(.33333333, 0),
    new Vector2(.33333333, .5),

    new Vector2(.33333333, 0),
    new Vector2(.33333333, .5),
    new Vector2(.66666666, .0),
    new Vector2(.66666666, .5),

    new Vector2(.66666666, 0),
    new Vector2(.66666666, .5),
    new Vector2(1, 0),
    new Vector2(1, .5),

    new Vector2(0, .5),
    new Vector2(0, 1),
    new Vector2(.33333333, .5),
    new Vector2(.33333333, 1),

    new Vector2(.33333333, .5),
    new Vector2(.33333333, 1),
    new Vector2(.66666666, .5),
    new Vector2(.66666666, 1),

    new Vector2(.66666666, .5),
    new Vector2(.66666666, 1),
    new Vector2(1, .5),
    new Vector2(1, 1),
  ]

  triangles: number[] = [
    2, 1, 0,
    1, 2, 3,

    6, 5, 4,
    5, 6, 7,

    10, 9, 8,
    9, 10, 11,

    14, 13, 12,
    13, 14, 15,

    18, 17, 16,
    17, 18, 19,

    22, 21, 20,
    21, 22, 23
  ]
}

class QuadMesh extends Mesh {
  vertices: Vector3[] = [
    new Vector3(-.5, .5, 0),
    new Vector3(-.5, -.5, 0),
    new Vector3(.5, .5, 0),
    new Vector3(.5, -.5, 0)
  ]

  uv: Vector2[] = [
    new Vector2(0, 1),
    new Vector2(0, 0),
    new Vector2(1, 1),
    new Vector2(1, 0)
  ]

  triangles: number[] = [
    2, 1, 0,
    1, 2, 3
  ]
}

class SphereMesh extends Mesh {
  constructor() {
    super()

    this.vertices = [
      ...this.subdivision([Vector3.up, Vector3.forward, Vector3.right], 0),
      ...this.subdivision([Vector3.up, Vector3.right, Vector3.back], 0),
      ...this.subdivision([Vector3.up, Vector3.back, Vector3.left], 0),
      ...this.subdivision([Vector3.up, Vector3.left, Vector3.forward], 0),
      ...this.subdivision([Vector3.down, Vector3.right, Vector3.forward], 0),
      ...this.subdivision([Vector3.down, Vector3.forward, Vector3.left], 0),
      ...this.subdivision([Vector3.down, Vector3.left, Vector3.back], 0),
      ...this.subdivision([Vector3.down, Vector3.back, Vector3.right], 0),
    ]

    this.triangles = range(0, this.vertices.length)

    // acos 0 ~ PI
    this.uv = this.vertices.map((vertex, index, arr) => {
      const theta = acos(vertex.y)

      if (theta == 0) {
        return new Vector2(0, vertex.y > 0 ? 1 : 0)
      }

      // FIXME
      if (vertex.z == 0 && vertex.x > 0) {
        if (vertex.y >= 0) {
          // 上半球 这里没等于
          return arr[index + 1].z > 0
            ? new Vector2(0, (PI - theta) / PI)
            : new Vector2(1, (PI - theta) / PI)
        } else {
          // 下半球 这里有等于
          return arr[index + 1].z >= 0
            ? new Vector2(0, (PI - theta) / PI)
            : new Vector2(1, (PI - theta) / PI)
        }
      }

      const phi = vertex.z > 0
        ? clamp(acos(clamp(vertex.x / sin(theta), -1, 1)), 0, PI)
        : 2 * PI - clamp(acos(clamp(vertex.x / sin(theta), -1, 1)), 0, PI)

      return new Vector2(phi / (2 * PI), (PI - theta) / PI)
    })
  }

  // 细分
  private subdivision(vertices: Vector3[], level: number, maxLevel: number = 3): Vector3[] {
    if (level > maxLevel) return vertices

    level += 1

    const A = vertices[0]
    const B = vertices[1]
    const C = vertices[2]

    const mAB = Vector3.lerp(A, B, .5).normalize()
    const mAC = Vector3.lerp(A, C, .5).normalize()
    const mBC = Vector3.lerp(B, C, .5).normalize()

    return [
      ...this.subdivision([A, mAB, mAC], level),
      ...this.subdivision([mAB, B, mBC], level),
      ...this.subdivision([mAC, mBC, C], level),
      ...this.subdivision([mAB, mBC, mAC], level)
    ]
  }
}

export const cube = new CubeMesh()
export const sphere = new SphereMesh()
export const quad = new QuadMesh()

export {
  Mesh,
  CubeMesh,
  QuadMesh,
  SphereMesh
}