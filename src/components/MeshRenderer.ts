import GameObject from '../game-objects/GameObject'
import Material from '../components/Material'

class MeshRenderer {
  gameObject: GameObject
  material: Material
}

export default MeshRenderer