import Component from './Component'
import { Mesh } from './Mesh'
import GameObject from '../game-objects/GameObject'

class MeshFilter extends Component {
  mesh: Mesh
  gameObject: GameObject
}

export default MeshFilter