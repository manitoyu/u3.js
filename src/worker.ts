import RasterizerWorker from './engine/RasterizerWorker'
import Camera from './game-objects/Camera'
import Quaternion from './core/Quaternion'
import Vector2 from './core/Vector2'
import Vector3 from './core/Vector3'
import Vector4 from './core/Vector4'
import { VertexOut } from './core/Structs'

const rasterizer = new RasterizerWorker()

declare function postMessage(x: any): void

interface SerializableVertex {
  mvPosition: {
    x: number
    y: number
    z: number
    w: number
  }
  normal: {
    x: number
    y: number
    z: number
  }
  position: {
    x: number
    y: number
    z: number
    w: number
  }
  texcoord: {
    x: number
    y: number
  }
}

Camera.main.fieldOfView = 60
Camera.main.pixelWidth = 800
Camera.main.pixelHeight = 600
Camera.main.aspect = Camera.main.pixelWidth / Camera.main.pixelHeight
Camera.main.nearClipPlane = .3
Camera.main.farClipPlane = 1000
Camera.main.transform.rotation = Quaternion.euler(0, 0, 0)
Camera.main.transform.position = new Vector3(0, 0, -2)
Camera.main.render()

onmessage = e => {
  const fragment: VertexOut[] = e.data.map((vertex: SerializableVertex) => ({
    mvPosition: new Vector4(vertex.mvPosition.x, vertex.mvPosition.y, vertex.mvPosition.z, vertex.mvPosition.w),
    normal: new Vector3(vertex.normal.x, vertex.normal.y, vertex.normal.z),
    position: new Vector4(vertex.position.x, vertex.position.y, vertex.position.z, vertex.position.w),
    texcoord: new Vector2(vertex.texcoord.x, vertex.texcoord.y)
  }))

  rasterizer.lerp(fragment, o => {
    postMessage(o)
  })

  postMessage('ok')
}